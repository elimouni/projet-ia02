from typing import List, Tuple
import subprocess
from itertools import combinations
import os

# Function to get the CNF combinations for each square for field type (ground, sea) and animals
# 5 x square_number + 1 : square is sea
# 5 x square_number + 2 : square is land
# 5 x square_number + 3 : square has a croco
# 5 x square_number + 4 : square has a tiger
# 5 x square_number + 5 : square has a shark
def initialClauses(length: int, width: int) -> List[Tuple[int]]:
    # for each square of the grid
    result = []
    for i in range(length * width):
        # law 1 : unique(sea, ground) + at_least_one(sea, ground)
        for val in unique([5*i+1, 5*i+2]):
            result.append(val)
        # law 4 : max_one(croco,tiger,shark)
        for val in max_one([5*i+3, 5*i+4, 5*i+5]):
            result.append(val)
        # law 3 : if ground then not shark (TODO: verify if shark-> sea)
        for val in max_one([5*i+2, 5*i+5]):
            result.append(val)
        # law 3 : if sea then not tiger (TODO: verify if tiger-> land)
        for val in max_one([5*i+1, 5*i+4]):
            result.append(val)
    
    return result

# Function to add max of each animal / land to dimacs file
def addMaxToDimacs(length: int, width: int, nb_sharks: int, nb_crocos: int, nb_tigers: int, nb_land: int, nb_sea: int) -> List[Tuple[int]]:
    # for each square of the grid
    result = []
    sea_all_squares = []
    land_all_squares = []
    croco_all_squares = []
    tiger_all_squares = []
    shark_all_squares = []
    for i in range(length * width):
        # preparing arrays for law 5
        sea_all_squares.append(5*i+1)
        land_all_squares.append(5*i+2)
        croco_all_squares.append(5*i+3)
        tiger_all_squares.append(5*i+4)
        shark_all_squares.append(5*i+5)

    # Verifying the max amount of crocos, tigers, and sharks
    if (abs(nb_sharks)+abs(nb_crocos)+abs(nb_tigers) <= length * width) and (abs(nb_land)+abs(nb_sea) == length * width):
        # law 5 : including the max croco amount
        for val in exactly_n(croco_all_squares, nb_crocos):
            result.append(val)
        # law 5 : including the max tiger amount
        for val in exactly_n(tiger_all_squares, nb_tigers):
            result.append(val)
        # law 5 : including the max shark amount
        for val in exactly_n(shark_all_squares, nb_sharks):
            result.append(val)
        # law 5 : including the max sea amount
        for val in exactly_n(sea_all_squares, nb_sea):
            result.append(val)
        # law 5 : including the max land amount
        for val in exactly_n(land_all_squares, nb_land):
            result.append(val)
    return result

# Function to get all the adjacent squares for each square of the grid
def getNearBySquares(length: int, width: int):
    max_size = length * width
    adjacentes = dict()
    for i in range(length * width):
        table = []
        if(i-1>=0 and (i-1)%width<i%width):
            table.append(i-1)
        if(i+1< max_size and (i+1)%width>i%width):
            table.append(i+1)
        if(i-width-1>=0 and (i-1)%width<i%width):
            table.append(i-width-1)
        if(i-width>=0):
            table.append(i-width)
        if(i-width+1>=0 and (i+1)%width>i%width):
            table.append(i-width+1)
        if(i+width-1 < max_size and (i-1)%width<i%width):
            table.append(i+width-1)
        if(i+width < max_size):
            table.append(i+width)
        if(i+width+1 < max_size and (i+1)%width>i%width):
            table.append(i+width+1)
        adjacentes[i]=table

    return adjacentes

# Function to get the CNF combinations from a square after discovering it to add it to the dimacs file
def getDimacsFromSquare(length: int, width: int):
    max_size = length * width
    adjacentes = dict()
    for i in range(length * width):
        table = []
        if(i-1>=0 and (i-1)%width<i%width):
            table.append(i-1)
        if(i+1< max_size and (i+1)%width>i%width):
            table.append(i+1)
        if(i-width-1>=0 and (i-1)%width<i%width):
            table.append(i-width-1)
        if(i-width>=0):
            table.append(i-width)
        if(i-width+1>=0 and (i+1)%width>i%width):
            table.append(i-width+1)
        if(i+width-1 < max_size and (i-1)%width<i%width):
            table.append(i+width-1)
        if(i+width < max_size):
            table.append(i+width)
        if(i+width+1 < max_size and (i+1)%width>i%width):
            table.append(i+width+1)
        adjacentes[i]=table
    return adjacentes

# Function to get all the nearby squares from a square
def getNearbySquares(m : int, n : int, length : int, width : int):
    max_size = length * width
    # i = n * m
    i = n * width + m
    table = []
    if(i-1>=0 and (i-1)%width<i%width):
        table.append(i-1)
    if(i+1< max_size and (i+1)%width>i%width):
        table.append(i+1)
    if(i-width-1>=0 and (i-1)%width<i%width):
        table.append(i-width-1)
    if(i-width>=0):
        table.append(i-width)
    if(i-width+1>=0 and (i+1)%width>i%width):
        table.append(i-width+1)
    if(i+width-1 < max_size and (i-1)%width<i%width):
        table.append(i+width-1)
    if(i+width < max_size):
        table.append(i+width)
    if(i+width+1 < max_size and (i+1)%width>i%width):
        table.append(i+width+1)
    return table

# Function to get the CNF combinations when at least one element of the list must be true
def at_least_one(vars: List[int]) -> List[int]:
    result = []
    for values in vars:
        result.append(values)
    return result

# Function to get the CNF combinations when one element of the list must be true
def unique(vars: List[int]) -> List[List[int]]:
    result = []
    result.append(at_least_one(vars))
    for val in max_one(vars):
        result.append(val)
    return result

# Function to get the CNF combinations when at max one element of the list must be true
def max_one(vars: List[int]) -> List[List[int]]:
    result = []
    for a,b in combinations(vars, 2):
        result.append([-a, -b])
    return result

# Function to get the CNF combinations when at least n elements of the list must be true
def at_least_n(vars: List[int], n: int) -> List[int]:
    result = []
    size = len(vars)
    temp = combinations(vars, size-(n-1))
    for a in temp:
        oneTab = []
        for b in a:
            oneTab.append(b)
        result.append(oneTab)
    return result

# Function to get the CNF combinations when at max n elements of the list must be true
def max_n(vars: List[int], n: int) -> List[List[int]]:
    result = []
    size = len(vars)
    n_bar = size - n
    temp = combinations(vars, size-(n_bar-1))
    for a in temp:
        oneTab = []
        for b in a:
            oneTab.append(-b)
        result.append(oneTab)
    return result

# Function to get the CNF combinations when n elements of the list must be true
def exactly_n(vars: List[int], n: int) -> List[List[int]]:
    result = []
    for val in at_least_n(vars, n):
        result.append(val)
    for val in max_n(vars, n):
        result.append(val)
    return result

# Function to get the CNFs when discovering a square
def getCNFFromSquare(x : int, y : int, length : int, width : int, landType : str, animal : str, nearbyCroco : int, nearbyTigers : int, nearbySharks : int) -> List[List[int]]:
    toAdd = []
    # Adding the value of the actual discovered square
    refVal = y*width*5+x*5
    if landType == 'S':
        toAdd.append([(refVal+1)])
        toAdd.append([-(refVal+2)])
    elif landType == 'L':
        toAdd.append([-(refVal+1)])
        toAdd.append([(refVal+2)])
    
    if animal == 'C':
        toAdd.append([(refVal+3)])
        toAdd.append([-(refVal+4)])
        toAdd.append([-(refVal+5)])
    elif animal == 'T':
        toAdd.append([-(refVal+3)])
        toAdd.append([(refVal+4)])
        toAdd.append([-(refVal+5)])
    elif animal == 'S':
        toAdd.append([-(refVal+3)])
        toAdd.append([-(refVal+4)])
        toAdd.append([(refVal+5)])
    else:
        toAdd.append([-(refVal+3)])
        toAdd.append([-(refVal+4)])
        toAdd.append([-(refVal+5)])
    
    # Adding constraints on nearby squares
    nearbySquares = getNearbySquares(x, y, length, width)
    for x in exactly_n([x*5+3 for x in nearbySquares], nearbyCroco if nearbyCroco != None else 0):
            toAdd.append(x)
    for x in exactly_n([x*5+5 for x in nearbySquares], nearbySharks if nearbySharks != None else 0):
            toAdd.append(x)
    for x in exactly_n([x*5+4 for x in nearbySquares], nearbyTigers if nearbyTigers != None else 0):
            toAdd.append(x)
    
    return toAdd

# Function to get the CNFs when wanting to make a test
def getCNFForSquare(x : int, y : int, width : int, landType : str, animal : str) -> List[List[int]]:
    toAdd = []
    # Adding the value of the actual discovered square
    refVal = y*width+x*5
    if landType == 'S':
        toAdd.append([(refVal+1)])
        toAdd.append([-(refVal+2)])
    elif landType == 'L':
        toAdd.append([-(refVal+1)])
        toAdd.append([(refVal+2)])
    
    if animal == 'C':
        toAdd.append([(refVal+3)])
        toAdd.append([-(refVal+4)])
        toAdd.append([-(refVal+5)])
    elif animal == 'T':
        toAdd.append([-(refVal+3)])
        toAdd.append([(refVal+4)])
        toAdd.append([-(refVal+5)])
    elif animal == 'S':
        toAdd.append([-(refVal+3)])
        toAdd.append([-(refVal+4)])
        toAdd.append([(refVal+5)])
    else:
        toAdd.append([-(refVal+3)])
        toAdd.append([-(refVal+4)])
        toAdd.append([-(refVal+5)])

    return toAdd

# Function to translate the CNFs to the dimacs format without the header
def cnfToDimacsWithoutHeader(clauses: List[List[int]]) -> str:
    inter=""
    inter2=str()
    for clause in clauses:
        inter=""
        count = 0
        for variable in clause:
            if count == 0:
                inter=str(variable)
            else:
                inter=inter+" "+str(variable)
            count +=1
        inter2=inter2 + inter + " 0\n"
    return inter2

# Function to translate the CNFs to the dimacs format
def cnfToDimacs(clauses: List[List[int]], nb_vars: int) -> str:
    p="p cnf "+str(nb_vars)
    inter=""
    inter2=""
    i=0
    for clause in clauses:
        i=i+1
        inter=""
        count = 0
        for variable in clause:
            if count == 0:
                inter=str(variable)
            else:
                inter=inter+" "+str(variable)
            count +=1
        inter2=inter2 + "\n" + inter + " 0"
    p=p+" "+str(i)+inter2 + "\n"
    return p

# Function to write the dimacs file
def writeDimacsFile(dimacs: str, filename: str):
    with open(filename, "w", newline="") as cnf:
        cnf.write(dimacs)

# Function to write one CNF in a dimacs file
def writeCNFDimacsFile(dimacs: str, filename: str, nlines : int):
    nlines = dimacs.count('\n')
    with open(filename, "r+", newline="") as file:
        # Change the amount of CNFs
        list_of_lines = file.readlines()
        list_of_lines[0] = list_of_lines[0].split(" ")[0] + " " + list_of_lines[0].split(" ")[1] + " " + list_of_lines[0].split(" ")[2] + " " + str(int(list_of_lines[0].split(" ")[3]) + nlines) + '\n'
        # Removing the last CNF
        file.seek(0)
        list_of_lines.append(dimacs)
        for line in list_of_lines:
            file.write(line)
        file.truncate()

# Function to delete one or more lines in the CNF of a dimacs file
def deleteCNFDimacsFile(filename: str, nlines : int):
    with open(filename, "r+", encoding = "utf-8", newline="") as file:
        # Change the amount of CNFs
        list_of_lines = file.readlines()
        list_of_lines[0] = list_of_lines[0].split(" ")[0] + " " + list_of_lines[0].split(" ")[1] + " " + list_of_lines[0].split(" ")[2] + " " + str(int(list_of_lines[0].split(" ")[3]) - nlines) + '\n'
        # Removing the last CNF
        file.seek(0)
        list_of_lines = list_of_lines[:len(list_of_lines)-nlines]
        for line in list_of_lines:
            file.write(line)
        file.truncate()

# Function to execute Gophersat, if mode equals to True -> gets models, if False then returns count
def exec_gophersat(
    filename: str, cmd: str = "gophersat", encoding: str = "utf8", mode: bool = True
) -> Tuple[bool, List[int]]:

    if mode == True:
        result = subprocess.run(
            [cmd, filename], capture_output=True, check=True, encoding=encoding
        )
    else:
        result = subprocess.run(
            [cmd,"-count", filename], capture_output=True, check=True, encoding=encoding
        )

    string = str(result.stdout)
    lines = string.splitlines()

    if mode:
        if lines[1] != "s SATISFIABLE":
            return False, []

        model = lines[2][2:].split(" ")
        return True, [int(x) for x in model]
    else:
        if lines[1] == "0":
            return False, [0]

        return True, [int(lines[1])]