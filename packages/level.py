from packages.crocomine_client import CrocomineClient
from typing import List
import random
import time
from packages.dimacs import cnfToDimacsWithoutHeader, deleteCNFDimacsFile, initialClauses, cnfToDimacs, writeDimacsFile, writeCNFDimacsFile, getCNFFromSquare, exec_gophersat, getNearbySquares
import sys

class Level:
    def __init__(self, width : int, length : int, croco : int, shark : int, tiger : int, land : int, sea : int, pathCNF : str, pathSolver : str, server : CrocomineClient = None):
        self.width = width
        self.length = length
        self.map = [[{'landType' : 0, 'animal' : 0, 'discovered' : False} for j in range (width)] for i in range(length)]
        self.totalCroco = croco
        self.totalShark = shark
        self.totalTiger = tiger
        self.totalLand = land
        self.totalSea = sea
        self.discoveredCroco = 0
        self.discoveredShark = 0
        self.discoveredTiger = 0
        self.discoveredLand = 0
        self.discoveredSea = 0
        self.resolved = False
        self.pathCNF = pathCNF
        self.pathSolver = pathSolver
        self.borderSquares = []
        self.server = server

    # function to initialize the dimacs file
    def initializeCNFFile(self):
        try:
            clauses = initialClauses(self.width, self.length)
            writeDimacsFile(cnfToDimacs(clauses, self.width*self.length*5), self.pathCNF)
            if self.discoveredSea == self.totalSea:
                self.maxLandTypeReached('S')
            if self.discoveredLand == self.totalLand:
                self.maxLandTypeReached('L')

            if self.discoveredCroco == self.totalCroco:
                self.maxAnimalReached('C')
            if self.discoveredShark == self.totalShark:
                self.maxAnimalReached('S')
            if self.discoveredTiger == self.totalTiger:
                self.maxAnimalReached('T')
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise

    # function to display the map
    def displayGrid(self):
        for line in self.map:
            for square in line:
                if square["landType"] == 0:
                    print('X', end ="")
                elif square["animal"] == 5:
                    print('S', end ="")
                elif square["animal"] == 4:
                    print('T', end ="")
                elif square["animal"] == 3:
                    print('C', end ="")
                elif square["landType"] == 1:
                    print("~", end ="")
                else:
                    print("\u25A0", end ="")
            print('\n', end ="")

    # function to add to the cnf file when an animal's max amount was reached
    def maxLandTypeReached(self, landType : str):
        indexDiscovered=[]
        if landType == 'L':
            countLine = 0
            for line in self.map:
                countCol = 0
                for square in line:
                    if square["landType"] != 0 and square["landType"] == 2:
                        indexDiscovered.append([(countLine * self.width * 5 + countCol * 5 + 2)])
                    else:
                        indexDiscovered.append([-(countLine * self.width * 5 + countCol * 5 + 2)])
                    countCol += 1
                countLine += 1
            dimacs = cnfToDimacsWithoutHeader(indexDiscovered)
            writeCNFDimacsFile(dimacs, self.pathCNF, len(indexDiscovered))
            return 1
        elif landType == 'S':
            countLine = 0
            for line in self.map:
                countCol = 0
                for square in line:
                    if square["landType"] != 0 and square["landType"] == 1:
                        indexDiscovered.append([(countLine * self.width * 5 + countCol * 5 + 1)])
                    else:
                        indexDiscovered.append([-(countLine * self.width * 5 + countCol * 5 + 1)])
                    countCol += 1
                countLine += 1
            dimacs = cnfToDimacsWithoutHeader(indexDiscovered)
            writeCNFDimacsFile(dimacs, self.pathCNF, len(indexDiscovered))
            return 1

    # function to add to the cnf file when an animal's max amount was reached
    def maxAnimalReached(self, animal : str):
        indexDiscovered=[]
        if animal == 'C':
            countLine = 0
            for line in self.map:
                countCol = 0
                for square in line:
                    if square["animal"] != 0 and square["animal"] == 3:
                        indexDiscovered.append([(countLine * self.width * 5 + countCol * 5 + 3)])
                    else:
                        indexDiscovered.append([-(countLine * self.width * 5 + countCol * 5 + 3)])
                    countCol += 1
                countLine += 1
            dimacs = cnfToDimacsWithoutHeader(indexDiscovered)
            writeCNFDimacsFile(dimacs, self.pathCNF, len(indexDiscovered))
            return 1
        elif animal == 'S':
            countLine = 0
            for line in self.map:
                countCol = 0
                for square in line:
                    if square["animal"] != 0 and square["animal"] == 5:
                        indexDiscovered.append([(countLine * self.width * 5 + countCol * 5 + 5)])
                    else:
                        indexDiscovered.append([-(countLine * self.width * 5 + countCol * 5 + 5)])
                    countCol += 1
                countLine += 1
            dimacs = cnfToDimacsWithoutHeader(indexDiscovered)
            writeCNFDimacsFile(dimacs, self.pathCNF, len(indexDiscovered))
            return 1
        elif animal == 'T':
            countLine = 0
            for line in self.map:
                countCol = 0
                for square in line:
                    if square["animal"] != 0 and square["animal"] == 4:
                        indexDiscovered.append([(countLine * self.width * 5 + countCol * 5 + 4)])
                    else:
                        indexDiscovered.append([-(countLine * self.width * 5 + countCol * 5 + 4)])
                    countCol += 1
                countLine += 1
            dimacs = cnfToDimacsWithoutHeader(indexDiscovered)
            writeCNFDimacsFile(dimacs, self.pathCNF, len(indexDiscovered))
            return 1

    # function to set the terrain type of an undiscovered square
    def setSquareLandType(self, x : int, y : int, landType : str):
        # Adding the terrain to the CNF file
        cnf = [[y*self.width*5+x*5 + 1]] if landType == 'S' else [[y*self.width*5+x*5 + 2]]
        dimacs = cnfToDimacsWithoutHeader(cnf)
        writeCNFDimacsFile(dimacs, self.pathCNF, 1)

        # Editing the map
        try:
            if landType == "L":
                self.discoveredLand += 1
                self.map[y][x]["landType"] = 2
            elif landType == "S":
                self.discoveredSea += 1
                self.map[y][x]["landType"] = 1
        except IndexError:
            print("Index out of range.")
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return 0

    # discovering a square and adding every info to the dimacs file
    def guessSquare(self, x : int, y : int, animal : str):
        # Adding the CNF to the file
        cnf = [[y*self.width*5+x*5+1]]
        if animal == "T":
            cnf[0][0]+=4
        elif animal == "S":
            cnf[0][0]+=5
        elif animal == "C":
            cnf[0][0]+=3
        
        dimacs = cnfToDimacsWithoutHeader(cnf)
        writeCNFDimacsFile(dimacs, self.pathCNF, 1)

        # Removing the actual object from the border, adding the other ones
        if self.getIndex(x,y) in self.borderSquares:
            self.borderSquares.remove(self.getIndex(x,y))
        self.addToBorder([x+1 for x in getNearbySquares(x, y, self.length, self.width)])

        # Editing the Map from object, and the amount of discovered animals / land type
        self.map[y][x]["discovered"] = True
        try:
            if animal == "T":
                self.discoveredTiger += 1
                self.map[y][x]["animal"] = 4
            elif animal == "S":
                self.discoveredShark += 1
                self.map[y][x]["animal"] = 5
            elif animal == "C":
                self.discoveredCroco += 1
                self.map[y][x]["animal"] = 3
        except IndexError:
            print("Index out of range.")
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
    
    # discovering a square and adding every info to the dimacs file
    def discoverSquare(self, x : int, y : int, animal : str, landType : str, nearbyCrocos: int, nearbySharks : int, nearbyTigers: int):
        # Adding the CNF to the file
        cnf = getCNFFromSquare(x, y, self.length, self.width, landType, animal, nearbyCrocos, nearbyTigers, nearbySharks)
        dimacs = cnfToDimacsWithoutHeader(cnf)
        writeCNFDimacsFile(dimacs, self.pathCNF, 5)

        # Removing the actual object from the border, adding the other ones
        if self.getIndex(x,y) in self.borderSquares:
            self.borderSquares.remove(self.getIndex(x,y))
        self.addToBorder([x+1 for x in getNearbySquares(x, y, self.length, self.width)])

        # Editing the Map from object, and the amount of discovered animals / land type
        self.map[y][x]["discovered"] = True
        try:
            if animal == "T":
                self.discoveredTiger += 1
                self.map[y][x]["animal"] = 4
            elif animal == "S":
                self.discoveredShark += 1
                self.map[y][x]["animal"] = 5
            elif animal == "C":
                self.discoveredCroco += 1
                self.map[y][x]["animal"] = 3

            if landType == "L":
                self.discoveredLand += 1
                self.map[y][x]["landType"] = 2
            elif landType == "S":
                self.discoveredSea += 1
                self.map[y][x]["landType"] = 1
        except IndexError:
            print("Index out of range.")
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
    
    # test how much probas a square has to be of a type
    def testCell(self, x : int, y : int, animal : str, mode = True)->int:
        # Need to add the negative of a the CNF to count the models
        factor = 0
        # If not a lot of squares to discover then we count the models
        if (self.totalLand + self.totalSea ) - (self.discoveredLand + self.discoveredLand) < 7:
            if animal != None:
                if animal == "C":
                    factor = 3
                if animal == "T":
                    factor = 4
                if animal == "S":
                    factor = 5

                cnf = [[-(y*self.width*5 + x*5 + factor)]]
                dimacs = cnfToDimacsWithoutHeader(cnf)
                writeCNFDimacsFile(dimacs, self.pathCNF, 1)
                try:
                    modelsCount = exec_gophersat(self.pathCNF, self.pathSolver, mode = False)
                except:
                    print("Le gophersat en PLS")
                    
                deleteCNFDimacsFile(self.pathCNF, 1)
            else:
                cnf = [[(y*self.width*5 + x*5 + 3), (y*self.width*5 + x*5 + 4), (y*self.width*5 + x*5 + 5)]]
                dimacs = cnfToDimacsWithoutHeader(cnf)
                writeCNFDimacsFile(dimacs, self.pathCNF, 1)
                try:
                    modelsCount = exec_gophersat(self.pathCNF, self.pathSolver, mode = False)
                except:
                    print("Le gophersat en PLS")

                deleteCNFDimacsFile(self.pathCNF, 1)
        else:
            if animal != None:
                if animal == "C":
                    factor = 3
                if animal == "T":
                    factor = 4
                if animal == "S":
                    factor = 5

                if self.map[y][x]["landType"] != 0:
                    if self.map[y][x]["landType"] == 1 and animal == 'T':
                        modelsCount = [True, [500000]]
                    elif self.map[y][x]["landType"] == 2 and animal == 'S':
                        modelsCount = [True, [500000]]
                    else:
                        cnf = [[-(y*self.width*5 + x*5 + factor)]]
                        dimacs = cnfToDimacsWithoutHeader(cnf)
                        writeCNFDimacsFile(dimacs, self.pathCNF, 1)
                        modelsCount = exec_gophersat(self.pathCNF, self.pathSolver)
                        if modelsCount[0] == False:
                            modelsCount = [False, [0]]
                        else:
                            modelsCount = [True, [1]]
                        deleteCNFDimacsFile(self.pathCNF, 1)
                else: 
                    cnf = [[-(y*self.width*5 + x*5 + factor)]]
                    dimacs = cnfToDimacsWithoutHeader(cnf)
                    writeCNFDimacsFile(dimacs, self.pathCNF, 1)
                    modelsCount = exec_gophersat(self.pathCNF, self.pathSolver)
                    if modelsCount[0] == False:
                        modelsCount = [False, [0]]
                    else:
                        modelsCount = [True, [1]]
                    deleteCNFDimacsFile(self.pathCNF, 1)
            else:
                cnf = [[(y*self.width*5 + x*5 + 3), (y*self.width*5 + x*5 + 4), (y*self.width*5 + x*5 + 5)]]
                dimacs = cnfToDimacsWithoutHeader(cnf)
                writeCNFDimacsFile(dimacs, self.pathCNF, 1)
                modelsCount = exec_gophersat(self.pathCNF, self.pathSolver)
                if modelsCount[0] == False:
                    modelsCount = [False, [0]]
                else:
                    modelsCount = [True, [1]]
                deleteCNFDimacsFile(self.pathCNF, 1)
            # print("Model count for [",x,',',y,'] for animal', animal,':', modelsCount)
        return modelsCount

    # function to get [x,y] based on an index and width
    def getCoordinates(self, index : int):
        index -= 1
        x = index%self.width
        y = index//self.width
        return x, y

    # function to get [x,y] based on an index and width
    def getIndex(self, x : int, y : int)-> int:
        index = y*self.width+x+1
        return index

    # function to discover randomly one undiscovered square
    def randomDiscover(self) -> int:
        index = 0
        undiscoveredMap = []
        index = 1
        for line in self.map:
            for square in line:
                if square["discovered"] == False:
                    # Factor 3 for the one which are not in the border
                    if index not in self.borderSquares:
                        undiscoveredMap.append(index)
                        undiscoveredMap.append(index)
                        undiscoveredMap.append(index)
                    else:
                        undiscoveredMap.append(index)
                index += 1
        print("choice in: ", undiscoveredMap)
        return random.choice(undiscoveredMap)

    # function to add squares to the border (line of undiscovered near discovered)
    def addToBorder(self, squares : List[int]):
        # if not discovered add to the border
        for square in squares:
            x, y = self.getCoordinates(square)
            # if not discovered and not in border then we add it to the border
            if square not in self.borderSquares and self.map[y][x]["discovered"] == False:
                self.borderSquares.append(square)

    # run using the [x,y] position from the first square to start
    def runWithoutServer(self, x : int, y : int):
        count = 0
        # TODO : DISCOVER THE FIRST SQUARE
        self.discoverSquare(x, y, None, 'L', 1, 0, 0)
        print("\n=======", count, "=======")
        self.displayGrid()
        # Number of resolve : width * lenght
        while self.resolved == False:
            count+=1
            statsBorder = dict()
            for j in self.borderSquares:
                # TODO : test here each square in border and discover / guess the most probable one
                x, y = self.getCoordinates(j)
                for animal in ['T', 'S', 'C', None]:
                    score = self.testCell(x, y, animal, None, True)
                    if j not in statsBorder or score < statsBorder[j]["score"]:
                        rep = {"animal": animal, "score": score}
                        statsBorder[j] = rep
                    if score[1] == False:
                        break
            
            print("border: ", self.borderSquares)
            toExplore = statsBorder[min(statsBorder.keys())]
            for temp in statsBorder:
                if statsBorder[temp]["score"] < toExplore["score"]:
                    toExplore = statsBorder[temp]
            
            # TODO : MAKE REQUEST HERE TO DISCOVER / TO GUESS
            discoveredLandType = 'L'
            nearbyCrocos = 0
            nearbySharks = 0
            nearbyTigers = 0
            self.discoverSquare(x, y, toExplore["animal"], discoveredLandType, nearbyCrocos, nearbySharks, nearbyTigers)
            # Displaying the grid at each discovering loop
            print("\n=======", count, "=======")
            self.displayGrid()

            # verifying if max animals / land are reached
            # TODO : ADD FOR THE OTHERS

            if self.discoveredSea == self.totalSea:
                self.maxLandTypeReached('S')
            if self.discoveredLand == self.totalLand:
                self.maxLandTypeReached('L')

            if self.discoveredCroco == self.totalCroco:
                self.maxAnimalReached('C')
            if self.discoveredShark == self.totalShark:
                self.maxAnimalReached('S')
            if self.discoveredTiger == self.totalTiger:
                self.maxAnimalReached('T')
            
            # If no border then the problem has been resolved
            if not self.borderSquares:
                self.resolved = True

    # run using the [x,y] position from the first square to start
    def run(self, x : int, y : int):
        # Initializing the CNF file
        self.initializeCNFFile()
        count = 0
        nearbyCrocos = 0
        nearbySharks = 0
        nearbyTigers = 0
        discoveredLandType = 'L'
        # TODO : DISCOVER THE FIRST SQUARE
        print("Discovering the first element of the grid")
        infos = self.server.discover(x, y)
        print("First element value: ", infos)
        
        if infos[0] == 'KO' or infos[0] == "Err":
            return 1
        elif infos[0] == 'GG':
            return 0
            
        # Add the data for each discovered square when discovering one (safe nearby are discovered at the same time)
        for discovered in infos[2]:
            nearbyCrocos = 0
            nearbySharks = 0
            nearbyTigers = 0
            x_pos = discovered["pos"][1]
            y_pos = discovered["pos"][0]
            discoveredLandType = 'S' if discovered["field"] == 'sea' else 'L'
            if "prox_count" in discovered.keys():
                nearbyCrocos = discovered["prox_count"][2]
                nearbySharks = discovered["prox_count"][1]
                nearbyTigers = discovered["prox_count"][0]
                self.discoverSquare(x_pos, y_pos, None, discoveredLandType, nearbyCrocos, nearbySharks, nearbyTigers)
            else:
                self.setSquareLandType(x_pos, y_pos, discoveredLandType)
        
        print("\n=======", count, "=======")
        self.displayGrid()
        # Number of resolve : width * lenght
        while self.resolved == False:
            count+=1
            statsBorder = dict()
            for j in self.borderSquares:
                # TODO : test here each square in border and discover / guess the most probable one
                x, y = self.getCoordinates(j)
                for animal in ['S', 'T', 'C', None]:
                    score = self.testCell(x, y, animal, mode = False)
                    if j not in statsBorder or score[1][0] < statsBorder[j]["score"][1][0]:
                        rep = {"animal": animal, "score": score}
                        statsBorder[j] = rep
                    if score[0] == False:
                        break
            
            print("border: ", self.borderSquares)
            bestChoice = False
            toExplore = statsBorder[min(statsBorder.keys())]
            indexToExplore = min(statsBorder.keys())
            for temp in statsBorder:
                if statsBorder[temp]["score"][0] == False:
                    bestChoice = True
                if statsBorder[temp]["score"] < toExplore["score"]:
                    toExplore = statsBorder[temp]

            if bestChoice:
                print("A best solution was found. Taking it.")
                x = self.getCoordinates(indexToExplore)[1]
                y = self.getCoordinates(indexToExplore)[0]
                toExplore["animal"] = None
            else:
                print("No best solution was found. Discovering a random one.")
                indexToExplore = self.randomDiscover()
                x = self.getCoordinates(indexToExplore)[1]
                y = self.getCoordinates(indexToExplore)[0]
            
            # If not an animal then discover, else guess
            if toExplore["animal"] == None:
                print("Guessing a safe square on [",x,',', y,']')
                infos = self.server.discover(x, y)
                if infos[0] == 'KO' or infos[0] == "Err":
                    return 1
                elif infos[0] == 'GG':
                    return 0
                for discovered in infos[2]:
                    nearbyCrocos = 0
                    nearbySharks = 0
                    nearbyTigers = 0
                    x_pos = discovered["pos"][1]
                    y_pos = discovered["pos"][0]
                    discoveredLandType = 'S' if discovered["field"] == 'sea' else 'L'
                    if "prox_count" in discovered.keys():
                        nearbyCrocos = discovered["prox_count"][2]
                        nearbySharks = discovered["prox_count"][1]
                        nearbyTigers = discovered["prox_count"][0]
                        # If the newly discovered square, there can be an animal
                        if x_pos == x and y_pos ==y:
                            self.discoverSquare(x_pos, y_pos, None, discoveredLandType, nearbyCrocos, nearbySharks, nearbyTigers)
                        else:
                            self.discoverSquare(x_pos, y_pos, None, discoveredLandType, nearbyCrocos, nearbySharks, nearbyTigers)
                    else:
                        self.setSquareLandType(x_pos, y_pos, discoveredLandType)
            else:
                print("Guessing a ", toExplore["animal"], " on the square [",x,',', y,']')
                infos = self.server.guess(x, y, toExplore["animal"])
                self.guessSquare(y,x,toExplore["animal"])
                print(infos)
                for square in infos[2]:
                    x_pos = square["pos"][1]
                    y_pos = square["pos"][0]
                    landType = 'S' if square["field"] == 'sea' else 'L'
                    self.setSquareLandType(x_pos, y_pos, landType)
                if infos[0] == 'KO' or infos[0] == "Err":
                    return 1
                elif infos[0] == 'GG':
                    return 0
            # Displaying the grid at each discovering loop
            print("\n=======", count, "=======")
            self.displayGrid()

            # verifying if max animals / land are reached
            # TODO : ADD FOR THE OTHERS

            if self.discoveredSea == self.totalSea:
                self.maxLandTypeReached('S')
            if self.discoveredLand == self.totalLand:
                self.maxLandTypeReached('L')

            if self.discoveredCroco == self.totalCroco:
                self.maxAnimalReached('C')
            if self.discoveredShark == self.totalShark:
                self.maxAnimalReached('S')
            if self.discoveredTiger == self.totalTiger:
                self.maxAnimalReached('T')
            
            # If no border then the problem has been resolved
            if not self.borderSquares:
                self.resolved = True
    
