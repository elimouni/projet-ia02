from os import path
from packages.dimacs import cnfToDimacsWithoutHeader, initialClauses, cnfToDimacs, writeDimacsFile, writeCNFDimacsFile, \
    getCNFFromSquare
import sys
from pprint import pprint

from packages.crocomine_client import CrocomineClient
from packages.level import Level

class Runner:
    def __init__(self, server: str, group: str, members: str, dimacs: str, solver: str):
        self.server = server
        self.group = group
        self.members = members
        self.croco = CrocomineClient(server, group, members, password="dedicace_au_montenegero")
        self.dimacs = dimacs
        self.solver = solver
        self.score = 0

    def next_level(self):
        status, msg, grid_infos = self.croco.new_grid()
        print(grid_infos)
        if (msg != "toutes les cartes ont été jouées" and msg != "pas de grille en cours"):
            if (status == "KO" or status == "GG"):
                return 1
            self.level = Level(grid_infos['n'], grid_infos['m'], grid_infos['croco_count'], grid_infos['shark_count'],
                               grid_infos['tiger_count'], grid_infos['land_count'], grid_infos['sea_count'],
                               self.dimacs, self.solver, self.croco)
            coord_i = grid_infos["start"][0]  ## On récup le i du start dans la variable grid_infos
            coord_j = grid_infos["start"][1]  ## On récup le j du start dans la variable grid_infos
            print("Coord of first square: ", coord_i, coord_j)
            # print(status, msg)
            # pprint(grid_infos)
            result = self.level.run(coord_i, coord_j)
            if result == 0:
                self.score += 1
        if (msg == "toutes les cartes ont été jouées"):
            return self.score
        else:
            return 1

    def exam(self):
        while (self.next_level()):
            self.next_level()



