## Projet AI27 

Utilisation d'un solveur SAT pour résoudre un problème type démineur.

## Lancement du programme
Pour lancer le projet, il faut setup le projet dans _main.py_ pour choisir les paramètres. Si vous n'êtes pas sur linux, il faut également changer le chemin du solver dans l'objet Runner dans le fichier _main.py_.
```bash
python3 ./main.py
```

## Architecture du logiciel

Nous avons choisi une conception orientée objet pour notre projet.

Nous avons donc créé une classe Level, qui va prendre en attribut l'ensemble des informations disponibles pour une carte.
La classe contient une fonction run qui va tout d'abord créer un fichier CNF de base, puis qui va se charger de découvrir les cases d'une carte une par une en s'appuyant sur des tests sur le fichier CNF ainsi que les fonctions statiques du fichier _dimacs.py_. L'approche des tests se fait via un stockage des cases "border", qui seront testées à la chaine pour savoir laquelle a le plus de chances d'être couverte d'un animal précis ou non. (méthode __testCell()__ de la classe). En cas d'égalité (de cas non sûr), une case aléatoire non découverte est désignée pour être découverte.

Nous avons crée une classe Runner qui a en attribut le lien du serveur, les informations du groupe et les chemins vers le
gophersat le fichier CNF, et un attribut de type Level qui correspond au niveau courant. 
Cet objet runner instancié une seule fois, va parcourir et lancer à la chaine chacune des cartes.

## Points forts du projet 

Notre algorithme insère un grand nombre d'informations dans notre fichier CNF dès qu'elles sont déductibles, non seulement sur 
la case courante mais aussi sur ses voisines. De la même façon, dès qu'on trouve tous les animaux d'une espèce, cela est entré
dans notre CNF de sorte à minimiser le plus possible l'incertitude dans nos prédictions.

Aussi, avoir implémenté le programme avec une approche orientée objet permet d'avoir un code plus propre, plus lisible et plus ouvert à l'amélioration.
Les deux objets communiquent naturellement entre eux et il sera plus simple pour une personne extérieure au binôme de comprendre notre démarche.
## Points à améliorer

Nous avons un problème lorsqu'il s'agit de choisir le modèle le plus probable. En effet, il s'agit du modèle qui a le plus de chances de se réaliser car sa négation renvoie un nombre plus faible de modèles.

Pour être encore plus performant, notre programme gagnerait à être optimisé. En particulier, notre méthode ne nous permet pas de 
résoudre des grilles lorsque les dimensions deviennent trop grandes.
L'une des pistes à laquelle nous avions pensé mais que nous n'avons pas eu le temps de tester est de 
remplacer certaines tâches trop lourdes au gophersat par des calculs de probabilité en fonction du 
nombre de dangers à proximité.
