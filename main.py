from packages.dimacs import initialClauses, cnfToDimacs, writeDimacsFile, getDimacsFromSquare, deleteCNFDimacsFile
from itertools import combinations
from packages.level import Level
from packages.crocomine_client import CrocomineClient
from packages.runner import Runner
# size of the example map
m = 5
n = 5

# amount of land and sea
nb_land = 10
nb_sea = 2

# amount amount of each animals
nb_sharks = 1
nb_tigers = 2
nb_crocos = 2

# p1 = Level("John", 36)

# Getting the initial CNF combinations in dimacs format before starting the game
#print(initialClauses(m, n, nb_sharks, nb_crocos, nb_tigers, nb_land, nb_sea))
# dimacsBase = cnfToDimacs(initialClauses(m, n, nb_sharks, nb_crocos, nb_tigers, nb_land, nb_sea), m*n*5+5)
'''
level = Level(m, n, nb_crocos, nb_sharks, nb_tigers, nb_land, nb_sea, './dimacs/model.cnf', './solver/win64/gophersat-1.1.6.exe')
level.initializeCNFFile()

level.run(1, 1)
'''
# level.discoverSquare(1, 1, None, 'S')
# level.discoverSquare(0, 0, None, 'L', 1, None, None)
# level.discoverSquare(1, 0, None, 'L', 1, None, None)
# level.discoverSquare(2, 0, None, 'L', 1, None, None)
#deleteCNFDimacsFile('./dimacs/model.cnf', 5)
# level.displayGrid()

server = "http://croco.lagrue.ninja:80"
group = "Groupe 67"
members = "Elias et Mehdi"

# windows :
# a= Runner(server, group, members, './dimacs/model.cnf', './solver/win64/gophersat-1.1.6.exe')
a= Runner(server, group, members, './dimacs/model.cnf', './solver/linux64/gophersat-1.1.6')

code = a.exam()

print("Score: ", code)